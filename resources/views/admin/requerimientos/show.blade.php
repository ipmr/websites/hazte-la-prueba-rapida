<x-app-layout>
    <div class="mb-5">
    <a href="{{ route('requerimientos') }}"><< Volver a tabla de requerimientos</a>
    </div>
    @livewire('detalle-de-requerimiento', ['requerimiento' => $requerimiento])
</x-app-layout>