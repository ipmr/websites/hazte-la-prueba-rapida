<x-app-layout>
	<div class="mb-5">
        <a href="{{ route('paquetes') }}"><< Volver a tabla de paquetes</a>
    </div>
    @livewire('detalle-de-paquete', [
        
        'metodo' => 'crear',
        'paquete' => new App\Models\Paquete()
        
    ])
</x-app-layout>