@component('mail::message')
# Hola {{ $requerimiento->nombre_completo }}

Recibimos con éxito tu requerimiento de <b>{{ $requerimiento->paquete->nombre }}</b>.

Nos pondremos en contacto contigo en la brevedad posible a través del correo electrónico que nos proporcionaste:

<b>{{ $requerimiento->email }}</b>

Gracias,<br>
{{ config('app.name') }}
@endcomponent
