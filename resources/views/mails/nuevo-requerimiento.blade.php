@component('mail::message')
# Hola

Se ha recibido un nuevo requerimiento desde el portal "Hazte La Prueba Rapida".

<b>Nombre:</b> {{ $requerimiento->nombre_completo }} <br>
<b>E-mail:</b> {{ $requerimiento->email }} <br>
<b>Dirección:</b> {{ $requerimiento->direccion }} <br>
<b>Telefono:</b> {{ $requerimiento->telefono }} <br>
<b>Celular:</b> {{ $requerimiento->celular }} <br>
<b>Paquete:</b> {{ $requerimiento->paquete->nombre }} <br>

@component('mail::button', ['url' => route('login')])
Ir al tablero de requerimientos
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
