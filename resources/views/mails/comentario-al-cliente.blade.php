@component('mail::message')
# Hola {{ $nombre_del_cliente }}

Tienes un nuevo comentario a través de la plataforma "haztelapruebarapida.com"

"{{ $comentario }}"

Gracias,<br>
{{ config('app.name') }}
@endcomponent
