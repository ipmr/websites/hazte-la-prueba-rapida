<x-guest-layout>
    <section class="bg-blue-900 text-center md:text-left bg-overlay bg-fixed shadow-lg" style="background-image: url('{{ asset('img/banner.jpg') }}')">
        <div class="container mx-auto px-7 md:px-0">
            <div class="block md:flex items-center py-10 md:space-x-24">
                <div class="w-full md:w-1/2">
                    
                    <h1 class="text-white text-2xl md:text-3xl uppercase font-bold mb-15">{{ config('app.name') }}</h1>

                    <h1 class="text-3xl md:text-5xl text-white font-bold uppercase mb-7 leading-tight">PRUEBA PARA DETECCIÓN EMPIRIC DE ANTIGENO COVID-19 Eagle Dx</h1>
                    <h3 class="text-2xl font-bold text-white">Dispositivo de prueba rápida con muestra de saliva y nasofaríngeo.</h3>

                </div>
                <div class="w-full md:w-1/2 mt-7 md:mt-0">
                    @livewire('formulario-de-requerimiento')
                </div>
            </div>
        </div>
    </section>


    <section class="py-15 text-center md:text-left">

        <div class="container mx-auto px-7 md:px-0">
            <div class="block md:flex items-center md:space-x-15">
                <div class="w-full md:w-1/2">
                    
                    <p class="text-blue-900 mb-7 text-xl font-bold">PRUEBA PARA DETECCIÓN DE ANTÍGENO COVID-19</p>

                    <h1 class="text-3xl">
                        Consiga resultados 95% similares a los que puede obtener con una prueba molecular de pcr a un precio mucho menor sin equipos especializados ni costosos.
                    </h1>

                    <div class="flex items-center space-x-5 justify-center md:justify-start mt-10">
                        <img src="{{ asset('img/icono_1.png') }}" class="w-24 h-24" alt="">
                        <img src="{{ asset('img/icono_2.png') }}" class="w-24 h-24" alt="">
                    </div>
                </div>
                <div class="w-full md:w-1/2 ml-auto mt-7 md:mt-0">
                    <img src="{{ asset('/img/pruebas.png') }}" alt="" class="w-full h-auto">
                </div>
            </div>
        </div>

    </section>

    <section class="py-15 bg-blue-200">

        <div class="container mx-auto px-7 md:p-0">
            <div class="w-full md:w-2/3 mx-auto mb-12">
                <h1 class="text-3xl md:text-2xl font-bold text-center">
                    No requiere de instrumentos adicionales o especializados
                    y brinda los resultados en un tiempo de 15 minutos,
                    convirtiéndola en una herramienta masiva de detección
                    para su empresa.
                </h1>
            </div>
            <ul class="block text-center md:text-left md:flex items-center md:space-x-24 space-y-10 md:space-y-0 text-2xl md:text-xl">
                <li>Tendrá una herramienta diagnóstica fiable y de alto rendimiento con resultados rápidos.</li>
                <li>Es ideal para aplicarla en una gran variedad de escenarios dentro de su centro de trabajo.</li>
                <li>El tubo autónomo con hisopo “desprendible” minimiza la exposición de su personal.</li>
            </ul>
        </div>

    </section>

</x-guest-layout>