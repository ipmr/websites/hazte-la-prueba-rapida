<div>
    <form action="" wire:submit.prevent="actualizar_detalles_generales">
        <p><b>Detalles generales</b></p>
        <table class="table w-full text-left mb-12">
            <tr>
                <th width="15%">Nombre:</th>
                <td class="py-1">
                    <input type="text" wire:model.defer="nombre" class="input">
                </td>
            </tr>
            <tr>
                <th width="15%">Descripción:</th>
                <td class="py-1">
                    <input type="text" wire:model.defer="descripcion" class="input">
                </td>
            </tr>
            <tr>
                <th width="15%">Especificaciones:</th>
                <td class="py-1">
                    <input type="text" wire:model.defer="especificaciones" class="input">
                </td>
            </tr>
            <tr>
                <th width="15%">Precio en dólares:</th>
                <td class="py-1">
                    <input type="text" wire:model.defer="precio" class="input">
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    @if($informacion_actualizada)
                    <p class="text-green-700">La información ha sido actualizada</p>
                    @endif
                    <button type="submit" class="bg-blue-600 text-white px-2">Guardar</button>
                </td>
            </tr>
        </table>
    </form>
    @if($metodo == 'actualizar')
    <p><b>Contenido del paquete</b></p>
    <table class="table w-full text-left mb-12">
        <tr>
            <td class="py-1">
                @foreach($this->productos as $producto)
                <p>
                    ({{ $producto->cantidad }}) {{ $producto->nombre }}
                    <a href="#" class="text-red-500" wire:click.prevent="eliminar_producto({{$producto->id}})">(Eliminar)</a>
                </p>
                @endforeach
                <div>
                    @livewire('agregar-producto-a-paquete', [
                    'paquete' => $paquete
                    ])
                </div>
            </td>
        </tr>
    </table>
    <p><b>Eliminar paquete</b></p>
    <form action="" wire:submit.prevent="eliminar_paquete">
        <button type="submit" class="bg-red-600 text-white px-2" >Eliminar paquete</button>
    </form>
    @endif
</div>