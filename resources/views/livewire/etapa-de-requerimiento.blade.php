<div>
    <form action="" wire:submit.prevent="actualizar_etapa">
        <select name="" id="" wire:model="etapa_id" class="input">
            <option value="1">Pendiente</option>
            <option value="2">En proceso</option>
            <option value="3">Cerrado Perdido</option>
            <option value="4">Cerrado Ganado</option>
        </select>
        <button type="submit" class="bg-gray-500 px-2 text-white shadow-none outline-none mt-2">
            Actualizar
        </button>
        @if($this->actualizado)
            <p class="text-sm mt-2">La etapa del requerimiento ha sido actualizada.</p>
        @endif
    </form>
</div>
