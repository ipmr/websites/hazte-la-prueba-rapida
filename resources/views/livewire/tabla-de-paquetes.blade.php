<div>
    @if(count($paquetes))
    <table class="table w-full text-left">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Descripción</th>
                <th>Especificaciones</th>
                <th>Contenido</th>
                <th>Precio</th>
                <th class="text-right">Ver</th>
            </tr>
        </thead>
        <tbody>
            @foreach($paquetes as $paquete)
            <tr>
                <td>{{ $paquete->nombre }}</td>
                <td>{{ $paquete->descripcion }}</td>
                <td>{{ $paquete->especificaciones }}</td>
                <td>
                    @foreach($paquete->productos as $producto)

                    <p>({{ $producto->cantidad }}) - {{ $producto->nombre }}</p>

                    @endforeach
                </td>
                <td>{{ $paquete->precio }}</td>
                <td class="text-right">
                    <a href="{{ route('paquete', $paquete) }}">Ver detalles</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else
    No se ha agregado paquetes.
    @endif
</div>
