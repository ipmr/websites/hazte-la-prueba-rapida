<div>
    <h1 class="text-xl font-bold mb-7 uppercase">Requerimientos</h1>

    @if(count($requerimientos))
    <table class="table text-left w-full">
        <thead>
            <tr>
                <th>Número</th>
                <th>Fecha</th>
                <th>Nombre completo</th>
                <th>E-mail</th>
                <th>Dirección</th>
                <th>Teléfono</th>
                <th>Celular</th>
                <th>Paquete</th>
                <th>Etapa</th>
                <th class="text-right">Ver</th>
            </tr>
        </thead>

        <tbody>
            @foreach($requerimientos as $requerimiento)
            <tr>
                <td>
                    <a class="font-bold text-red-500 hover:text-red-600" href="{{ route('requerimiento', $requerimiento) }}">
                    {{ $requerimiento->numero }}
                    </a>
                </td>
                <td>{{ $requerimiento->fecha }}</td>
                <td>{{ $requerimiento->nombre_completo }}</td>
                <td>{{ $requerimiento->email }}</td>
                <td>{{ $requerimiento->direccion }}</td>
                <td>{{ $requerimiento->telefono }}</td>
                <td>{{ $requerimiento->celular }}</td>
                <td>{{ $requerimiento->paquete->nombre }}</td>
                <td>{{ $requerimiento->etapa }}</td>
                <td class="text-right">
                    <a href="{{ route('requerimiento', $requerimiento->id) }}">Ver detalles</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else
    No hay requerimientos por el momento.
    @endif
</div>
