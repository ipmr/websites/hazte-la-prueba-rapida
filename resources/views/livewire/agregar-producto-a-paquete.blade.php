<div>

    @if(!$mostrar_formulario)
        <a href="#" class="bg-gray-600 text-white hover:text-white px-2" wire:click.prevent="agregar_producto">
            Agregar producto
        </a>
    @else
        <form action="" wire:submit.prevent="guardar_producto_en_paquete">
            <input type="text" class="input" wire:model="cantidad" placeholder="Cantidad">
            <select name="" id="" class="input my-2" wire:model="producto_id">
                <option disabled selected value="">Elige un producto</option>
                @foreach($productos as $producto)

                <option value="{{ $producto->id }}">{{ $producto->nombre }}</option>

                @endforeach
            </select>
            <div class="flex items-center">
                <button type="submit" class="bg-blue-600 hover:bg-blue-700 text-white px-2 mr-2">Agregar</button>
                <a href="#" class="bg-gray-600 text-white hover:text-white px-2" wire:click.prevent="cancelar">Cancelar</a>
            </div>
        </form>
    @endif
</div>
