<div>
    <form action="" class="py-2" wire:submit.prevent="enviar_correo_al_cliente">

        <textarea placeholder="Escribe tu comentario aquí..." wire:model.defer="comentario" name="" id="" rows="3" class="input"></textarea>
        
        @if($comentario_enviado)
        <p class="mb-2 text-sm">Se ha enviado el comentario a {{ $requerimiento->nombre_completo }}</p>
        @endif

        <button type="submit" wire:loading.attr="disabled" class="bg-blue-700 disabled:opacity-50 text-white px-2 mt-2">Enviar comentario</button>

    </form>
</div>
