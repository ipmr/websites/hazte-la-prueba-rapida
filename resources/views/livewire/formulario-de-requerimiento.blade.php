<div class="bg-white text-left p-5 md:py-10 md:px-12 rounded-xl shadow-lg">
    <h4 class="text-2xl font-bold uppercase text-center mb-5 text-blue-900">¡Solicita tus pruebas hoy mismo!</h4>
                        
    <form action="" class="mt-6" wire:submit.prevent="enviar_requerimiento">

        @if($requerimiento_enviado)

            <p class="text-lg text-green-600 font-bold mb-7 text-center">
                Gracias por tu requerimiento, nos pondremos en contacto contigo en la brevedad posible.
            </p>

        @endif

        <div class="flex items-center space-x-7">
            <div class="form-group w-1/2">
                <label for="" required><span class="text-red-500">*</span>Nombre:</label>
                <input type="text" wire:model.defer="nombre" class="form-control" required>
                @if($errors->has('nombre'))
                    <span class="text-red-500 mt-2 font-bold text-sm">{{ $errors->first('nombre') }}</span>
                @endif
            </div>
            <div class="form-group w-1/2">
                <label for=""><span class="text-red-500">*</span>Apellido:</label>
                <input type="text" wire:model.defer="apellido" class="form-control" required>
                @if($errors->has('apellido'))
                    <span class="text-red-500 mt-2 font-bold text-sm">{{ $errors->first('apellido') }}</span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for=""><span class="text-red-500">*</span>Correo electrónico:</label>
            <input type="email" wire:model.defer="email" class="form-control" required>
            @if($errors->has('email'))
                <span class="text-red-500 mt-2 font-bold text-sm">{{ $errors->first('email') }}</span>
            @endif
        </div>

        <div class="form-group">
            <label for=""><span class="text-red-500">*</span>Dirección:</label>
            <input type="text" wire:model.defer="direccion" class="form-control" required>
            @if($errors->has('direccion'))
                <span class="text-red-500 mt-2 font-bold text-sm">{{ $errors->first('direccion') }}</span>
            @endif
        </div>

        <div class="flex items-center space-x-7">
            <div class="form-group w-1/2">
                <label for="">Teléfono:</label>
                <input type="text" wire:model.defer="telefono" class="form-control">
                @if($errors->has('telefono'))
                    <span class="text-red-500 mt-2 font-bold text-sm">{{ $errors->first('telefono') }}</span>
                @endif
            </div>
            <div class="form-group w-1/2">
                <label for=""><span class="text-red-500">*</span>Celular:</label>
                <input type="text" wire:model.defer="celular" class="form-control" required>
                @if($errors->has('celular'))
                    <span class="text-red-500 mt-2 font-bold text-sm">{{ $errors->first('celular') }}</span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for=""><span class="text-red-500">*</span>Paquete:</label>
            <select name="paquete_id" class="form-control" wire:model.defer="paquete_id" required>
                <option disabled selected value="">Elige un paquete</option>
                @foreach($paquetes as $paquete)

                <option value="{{ $paquete->id }}">{{ $paquete->nombre }} | {{ $paquete->formato_de_precio }}</option>
                
                @endforeach
            </select>
            @if($errors->has('paquete_id'))
                <span class="text-red-500 mt-2 font-bold text-sm">{{ $errors->first('paquete_id') }}</span>
            @endif
        </div>

        <button type="submit" 
            class="submit-btn">
            Enviar requerimiento
        </button>

    </form>
</div>
