<div>

    <h1 class="text-xl font-bold mb-7 uppercase">Detalles de requerimiento</h1>

    <table class="table w-full text-left">

        <tbody>
            <tr>
                <th width="15%">Número:</th>
                <th class="font-bold text-red-500">{{ $requerimiento->numero }}</th>
            </tr>
            <tr>
                <th>Fecha:</th>
                <td>{{ $requerimiento->fecha }}</td>
            </tr>
            <tr>
                <th>Nombre completo:</th>
                <td>{{ $requerimiento->nombre_completo }}</td>
            </tr>
            <tr>
                <th>E-mail:</th>
                <td>{{ $requerimiento->email }}</td>
            </tr>
            <tr>
                <th>Teléfono:</th>
                <td>{{ $requerimiento->telefono }}</td>
            </tr>
            <tr>
                <th>Celular:</th>
                <td>{{ $requerimiento->celular }}</td>
            </tr>
            <tr>
                <th>Dirección:</th>
                <td>{{ $requerimiento->direccion }}</td>
            </tr>
            <tr>
                <th>Paquete:</th>
                <td>{{ $requerimiento->paquete->nombre }}</td>
            </tr>
            <tr>
                <th>Precio:</th>
                <td>${{ number_format($requerimiento->paquete->precio, 2) }} dls</td>
            </tr>
            <tr>
                <th>Etapa:</th>
                <td class="py-2">
                    @livewire('etapa-de-requerimiento', [
                        'requerimiento' => $requerimiento    
                    ])
                </td>
            </tr>
            <tr>
                <th>Comentario al cliente:</th>
                <td>
                    @livewire('comentario-al-cliente', [
                        'requerimiento' => $requerimiento    
                    ])
                </td>
            </tr>
        </tbody>

    </table>
</div>
