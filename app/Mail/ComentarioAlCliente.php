<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ComentarioAlCliente extends Mailable
{
    use Queueable, SerializesModels;

    public $request;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Tienes un nuevo comentario de Hazte La Prueba Rápida')
            ->markdown('mails.comentario-al-cliente', [

                'nombre_del_cliente' => $this->request['nombre_del_cliente'],
                'comentario' => $this->request['comentario']

            ]);
    }
}
