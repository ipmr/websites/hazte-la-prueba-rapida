<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Mail;

class ComentarioAlCliente extends Component
{

    public $comentario;
    public $requerimiento;
    public $comentario_enviado = false;

    public function mount($requerimiento){
        $this->requerimiento = $requerimiento;
    }

    public function render()
    {
        return view('livewire.comentario-al-cliente');
    }

    public function enviar_correo_al_cliente(){
    
        $datos_del_correo = [

            'nombre_del_cliente' => $this->requerimiento->nombre_completo,
            'comentario' => $this->comentario

        ];

        Mail::to($this->requerimiento->email)
            ->send(new \App\Mail\ComentarioAlCliente($datos_del_correo));

        $this->comentario = '';
        $this->comentario_enviado = true;

    }

}
