<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Requerimiento;

class TablaDeRequerimientos extends Component
{

    public function render()
    {

        $requerimientos = Requerimiento::orderBy('created_at', 'desc')->get();

        return view('livewire.tabla-de-requerimientos', compact('requerimientos'));
    }
}
