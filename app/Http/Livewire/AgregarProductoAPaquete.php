<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Producto;
use App\Models\ProductoEnPaquete;

class AgregarProductoAPaquete extends Component
{

    public $paquete;
    public $producto_id = '';
    public $cantidad = 1;

    public $mostrar_formulario = false;

    public function mount($paquete){
        $this->paquete = $paquete;
    }

    public function render()
    {
        $productos = Producto::all();
        return view('livewire.agregar-producto-a-paquete', compact('productos'));
    }

    public function agregar_producto(){
        $this->mostrar_formulario = true;
    }

    public function cancelar(){
        $this->mostrar_formulario = false;
        $this->producto_id = '';
        $this->cantidad = 1;
    }

    public function guardar_producto_en_paquete(){
        $producto_en_paquete = new ProductoEnPaquete();
        $producto_en_paquete->paquete_id = $this->paquete->id;
        $producto_en_paquete->producto_id = $this->producto_id;
        $producto_en_paquete->cantidad = $this->cantidad;
        $producto_en_paquete->save();

        return redirect()->route('paquete', $this->paquete->id);
    }

}
