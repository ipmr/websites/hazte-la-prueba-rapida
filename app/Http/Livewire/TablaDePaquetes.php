<?php

namespace App\Http\Livewire;

use App\Models\Paquete;
use Livewire\Component;

class TablaDePaquetes extends Component
{

    public function render()
    {

        $paquetes = Paquete::get();

        return view('livewire.tabla-de-paquetes', compact('paquetes'));
    }
}
