<?php

namespace App\Http\Livewire;

use Livewire\Component;

class DetalleDeRequerimiento extends Component
{

    public $requerimiento;

    public function mount($requerimiento){
        $this->requerimiento = $requerimiento;
    }

    public function render()
    {
        return view('livewire.detalle-de-requerimiento');
    }
}
