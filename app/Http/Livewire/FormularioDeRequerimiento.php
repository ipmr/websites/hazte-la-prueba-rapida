<?php

namespace App\Http\Livewire;

use Validator;
use App\Models\Paquete;
use Livewire\Component;
use App\Models\Requerimiento;
use App\Mail\NuevoRequerimiento;
use Illuminate\Support\Facades\Mail;
use App\Mail\GraciasPorTuRequerimiento;

class FormularioDeRequerimiento extends Component
{

    public $nombre = '';
    public $apellido = '';
    public $email = '';
    public $direccion = '';
    public $telefono = '';
    public $celular = '';
    public $paquete_id = '';

    public $requerimiento_enviado = false;

    public function render()
    {

        $paquetes = Paquete::get();

        return view('livewire.formulario-de-requerimiento', compact('paquetes'));
    }


    public function enviar_requerimiento(){

        $validar_datos = Validator::make([

            'nombre' => $this->nombre,
            'apellido' => $this->apellido,
            'email' => $this->email,
            'direccion' => $this->direccion,
            'telefono' => $this->telefono,
            'celular' => $this->celular,
            'paquete_id' => $this->paquete_id,

        ], [

            'nombre' => 'required|string',
            'apellido' => 'required|string',
            'email' => 'required|email',
            'direccion' => 'required|string',
            'telefono' => 'nullable|string',
            'celular' => 'required|string',
            'paquete_id' => 'required|exists:paquetes,id'

        ], [

            'nombre.required' => 'Por favor ingresa tu nombre',
            'apellido.required' => 'Por favor ingresa tu apellido',
            'email.required' => 'Por favor ingresa tu correo electrónico',
            'email.email' => 'Por favor ingresa un correo electrónico válido',
            'direccion.required' => 'Por favor ingresa tu dirección',
            'celular.required' => 'Por favor ingresa un número de celular',
            'paquete_id.required' => 'Por favor selecciona un paquete',
            'paquete_id.exists' => 'El paquete seleccionado no es válido',

        ])->validate();

        $requerimiento = $this->guardar_requerimiento();

        $this->enviar_notificaciones($requerimiento);

        $this->restaurar_campos();
    
    }

    public function guardar_requerimiento(){

        $requerimiento = new Requerimiento();
        $requerimiento->nombre = $this->nombre;
        $requerimiento->apellido = $this->apellido;
        $requerimiento->email = $this->email;
        $requerimiento->direccion = $this->direccion;
        $requerimiento->telefono = $this->telefono;
        $requerimiento->celular = $this->celular;
        $requerimiento->paquete_id = $this->paquete_id;

        $requerimiento->save();

        return $requerimiento;

    }

    public function enviar_notificaciones($requerimiento){

        $datos_de_mail = [

            'requerimiento' => $requerimiento

        ];

        Mail::to($requerimiento->email)
            ->send(new GraciasPorTuRequerimiento($datos_de_mail));

        Mail::to('admin@haztelapruebarapida.com')
            ->send(new NuevoRequerimiento($datos_de_mail));

    }

    public function restaurar_campos(){

        $this->nombre = '';
        $this->apellido = '';
        $this->email = '';
        $this->direccion = '';
        $this->telefono = '';
        $this->celular = '';
        $this->paquete_id = '';

        $this->requerimiento_enviado = true;

        $this->resetErrorBag();

    }

}
