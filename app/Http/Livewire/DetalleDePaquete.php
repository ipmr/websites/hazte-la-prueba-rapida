<?php

namespace App\Http\Livewire;

use App\Models\Paquete;
use App\Models\ProductoEnPaquete;
use Livewire\Component;

class DetalleDePaquete extends Component
{
    public $paquete;
    public $nombre;
    public $descripcion;
    public $especificaciones;
    public $precio;
    public $informacion_actualizada = false;
    public $metodo;

    public function getProductosProperty()
    {

        return $this->paquete->productos;

    }

    public function mount($paquete, $metodo)
    {
        $this->metodo           = $metodo;
        $this->paquete          = $paquete;
        $this->nombre           = $paquete->nombre;
        $this->descripcion      = $paquete->descripcion;
        $this->especificaciones = $paquete->especificaciones;
        $this->precio           = $paquete->precio;
    }

    public function render()
    {
        return view('livewire.detalle-de-paquete');
    }

    public function actualizar_detalles_generales()
    {
        $this->informacion_actualizada = false;

        $this->paquete->nombre           = $this->nombre;
        $this->paquete->descripcion      = $this->descripcion;
        $this->paquete->especificaciones = $this->especificaciones;
        $this->paquete->precio           = $this->precio;

        $this->paquete->save();

        if($this->metodo == 'crear'){
            return redirect()->route('paquete', $this->paquete->id);
        }else{
            $this->informacion_actualizada = true;
        }
    }

    public function eliminar_producto(ProductoEnPaquete $producto)
    {
        $paquete_id = $producto->paquete_id;
        $producto->delete();

        $paquete       = Paquete::find($paquete_id);
        $this->paquete = $paquete;
    }

    public function eliminar_paquete()
    {
        $this->paquete->delete();
        return redirect()->route('paquetes');
    }
}
