<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Validator;

class EtapaDeRequerimiento extends Component
{

    public $etapa_id;
    public $requerimiento;
    public $actualizado = false;

    public function mount($requerimiento){
        $this->requerimiento = $requerimiento;
        $this->etapa_id = $this->requerimiento->etapa_id;
    }

    public function render()
    {
        return view('livewire.etapa-de-requerimiento');
    }

    public function actualizar_etapa(){

        $validate = Validator::make([
            'etapa_id' => $this->etapa_id
        ],[

            'etapa_id' => 'required|in:1,2,3,4'

        ], [

            'etapa.required' => 'Por favor selecciona la etapa del requerimiento.',
            'etapa.in' => 'La etapa seleccionada no es válida.'

        ]);

        $this->requerimiento->etapa_id = $this->etapa_id;
        $this->requerimiento->save();

        $this->actualizado = true;

    }

}
