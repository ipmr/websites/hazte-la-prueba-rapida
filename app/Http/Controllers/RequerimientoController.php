<?php

namespace App\Http\Controllers;

use App\Models\Requerimiento;
use Illuminate\Http\Request;

class RequerimientoController extends Controller
{
    public function index(){
        return view('admin.requerimientos.index');
    }

    public function show(Requerimiento $requerimiento){
        return view('admin.requerimientos.show', compact('requerimiento'));
    }
}
