<?php

namespace App\Http\Controllers;

use App\Models\Paquete;
use Illuminate\Http\Request;

class PaqueteController extends Controller
{
    
    public function index(){
        return view('admin.paquetes.index');
    }

    public function show(Paquete $paquete){
        return view('admin.paquetes.show', compact('paquete'));
    }

    public function create(){
    	return view('admin.paquetes.create');
    }

}
