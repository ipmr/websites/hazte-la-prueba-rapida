<?php

namespace App\Http\Controllers;

use App\Models\ProductoEnPaquete;
use Illuminate\Http\Request;

class ProductoEnPaqueteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductoEnPaquete  $productoEnPaquete
     * @return \Illuminate\Http\Response
     */
    public function show(ProductoEnPaquete $productoEnPaquete)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductoEnPaquete  $productoEnPaquete
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductoEnPaquete $productoEnPaquete)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductoEnPaquete  $productoEnPaquete
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductoEnPaquete $productoEnPaquete)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductoEnPaquete  $productoEnPaquete
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductoEnPaquete $productoEnPaquete)
    {
        //
    }
}
