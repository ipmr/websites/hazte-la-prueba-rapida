<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paquete extends Model
{
    use HasFactory;

    public function productos(){
        return $this->hasMany(ProductoEnPaquete::class, 'paquete_id');
    }

    public function getFormatoDePrecioAttribute(){
        return '$' . number_format($this->precio, 2) . ' USD';
    }

}
