<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductoEnPaquete extends Model
{
    use HasFactory;

    public function paquete(){
        return $this->belongsTo(Paquete::class, 'paquete_id');
    }

    public function producto(){
        return $this->belongsTo(Producto::class, 'producto_id');
    }

    public function getNombreAttribute(){
        return $this->producto->nombre;
    }

}
