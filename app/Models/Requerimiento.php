<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Requerimiento extends Model
{
    use HasFactory;

    public function paquete(){
        return $this->belongsTo(Paquete::class, 'paquete_id');
    }

    public function getNumeroAttribute(){
        return '#'.(intval($this->id) + 1000);
    }

    public function getNombreCompletoAttribute(){
        return $this->nombre .' '. $this->apellido;
    }

    public function getFechaAttribute(){
        return $this->created_at->format('d M, Y');
    }

    public function getEtapaAttribute(){
        if($this->etapa_id == 1){
            return 'Pendiente';
        }elseif($this->etapa_id == 2){
            return 'En proceso';
        }elseif($this->etapa_id == 3){
            return 'Cerrado Perdido';
        }elseif($this->etapa_id == 4){
            return 'Cerrado Ganado';
        }
    }
}
