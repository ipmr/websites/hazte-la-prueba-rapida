<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory(1)->create([

            'name' => 'Administrador',
            'email' => 'admin@haztelapruebarapida.com',
            'password' => \Hash::make('hlpr@357')

        ]);
    }
}
