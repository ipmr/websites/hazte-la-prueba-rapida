<?php

namespace Database\Seeders;

use App\Models\Requerimiento;
use Illuminate\Database\Seeder;

class RequerimientoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Requerimiento::factory(10)->create();
    }
}
