<?php

namespace Database\Seeders;

use App\Models\Paquete;
use Illuminate\Database\Seeder;
use App\Models\ProductoEnPaquete;

class PaqueteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Paquete::factory(1)->create([

            'nombre' => '3 Pruebas Rápidas',
            'precio' => '60',

        ])->each(function($paquete){

            ProductoEnPaquete::factory(1)->create([
                'paquete_id' => $paquete->id,
                'producto_id' => 1,
                'cantidad' => 3
            ]);

        });;


        Paquete::factory(1)->create([

            'nombre' => '5 Pruebas Rápidas',
            'precio' => '100'

        ])->each(function($paquete){

            ProductoEnPaquete::factory(1)->create([
                'paquete_id' => $paquete->id,
                'producto_id' => 1,
                'cantidad' => 5
            ]);

        });

        Paquete::factory(1)->create([

            'nombre' => '10 Pruebas Rápidas',
            'precio' => '200'

        ])->each(function($paquete){

            ProductoEnPaquete::factory(1)->create([
                'paquete_id' => $paquete->id,
                'producto_id' => 1,
                'cantidad' => 10
            ]);

        });

        Paquete::factory(1)->create([

            'nombre' => '20 Pruebas Rápidas',
            'precio' => '400'

        ])->each(function($paquete){

            ProductoEnPaquete::factory(1)->create([
                'paquete_id' => $paquete->id,
                'producto_id' => 1,
                'cantidad' => 20
            ]);

        });

        Paquete::factory(1)->create([

            'nombre' => '50 Pruebas Rápidas',
            'precio' => '1000'

        ])->each(function($paquete){

            ProductoEnPaquete::factory(1)->create([
                'paquete_id' => $paquete->id,
                'producto_id' => 1,
                'cantidad' => 50
            ]);

        });


        Paquete::factory(1)->create([

            'nombre' => '100 Pruebas Rápidas',
            'precio' => '2000'

        ])->each(function($paquete){

            ProductoEnPaquete::factory(1)->create([
                'paquete_id' => $paquete->id,
                'producto_id' => 1,
                'cantidad' => 100
            ]);

        });
    }
}
