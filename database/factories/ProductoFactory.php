<?php

namespace Database\Factories;

use App\Models\Producto;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Producto::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => 'Prueba para Detección de Antigeno Covid-19 Eagle Dx',
            'descripcion' => 'No requiere de instrumentos adicionales o especializados y brinda los resultados en un tiempo de 15 minutos,
            convirtiéndola en una herramienta masiva de detección
            para su empresa.',
            'detalles' => '
                <h3>CONSIGA RESULTADOS 99.4 % SIMILARES A LOS QUE PUEDE OBTENER CON UNA PRUEBA
                MOLECULAR DE PCR, A UN PRECIO MUCHO MENOR, SIN EQUIPOS ESPECIALIZADOS Y
                COSTOSOS.</h3>
                <ul>
                    <li>Tendrá una herramienta diagnóstica fiable y de alto rendimiento con resultados rápidos.</li>
                    <li>Es ideal para aplicarla en una gran variedad de escenarios dentro de su centro de trabajo.</li>
                    <li>El tubo autónomo con hisopo “desprendible” minimiza la exposición de su personal.</li>
                </ul>
            ',
            'precio' => '20',
            'margen' => '0'
        ];
    }
}
