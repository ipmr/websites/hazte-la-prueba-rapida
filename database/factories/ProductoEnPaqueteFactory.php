<?php

namespace Database\Factories;

use App\Models\ProductoEnPaquete;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductoEnPaqueteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductoEnPaquete::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'paquete_id' => 1,
            'producto_id' => 1,
            'cantidad' => 1
        ];
    }
}
