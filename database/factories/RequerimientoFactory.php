<?php

namespace Database\Factories;

use App\Models\Requerimiento;
use Illuminate\Database\Eloquent\Factories\Factory;

class RequerimientoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Requerimiento::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->firstName,
            'apellido' => $this->faker->lastName,
            'email' => $this->faker->email,
            'telefono' => $this->faker->tollFreePhoneNumber,
            'celular' => $this->faker->tollFreePhoneNumber,
            'direccion' => $this->faker->address,
            'paquete_id' => rand(1,3),
        ];
    }
}
