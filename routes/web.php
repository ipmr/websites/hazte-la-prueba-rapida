<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PaqueteController;
use App\Http\Controllers\RequerimientoController;

Route::view('/', 'website.index')->name('home');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::group([
    'prefix' => 'admin',
    'middleware' => ['auth:sanctum', 'verified']
], function(){

    Route::get('/paquetes', [PaqueteController::class, 'index'])->name('paquetes');
    Route::get('/paquetes/{paquete}', [PaqueteController::class, 'show'])->name('paquete');
    Route::get('/nuevo-paquete', [PaqueteController::class, 'create'])->name('nuevo-paquete');
    Route::get('/', [RequerimientoController::class, 'index'])->name('requerimientos');
    Route::get('/{requerimiento}', [RequerimientoController::class, 'show'])->name('requerimiento');

});